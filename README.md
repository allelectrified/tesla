# Tesla Bugs

Identify/validate/refine problems in Tesla user interfaces and car behaviours (car UI, app, and AP). Click on "Issues" at left. All Tesla owners invited: sign up for Gitlab, then request access. Also a tips wiki. This is unaffiliated with Tesla.

From the menus, you can see 
 - the list of Issues (use this for bugs and feature suggestions).
 - the Wiki, which distills advice for Tesla owners.
 